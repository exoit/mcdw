using System.Security.Cryptography;
using System.Text;

namespace MCDw.Utilities
{
    public static class ChecksumUtility
    {
        public static string Md5(this byte[] data)
        {
            using (var md5 = MD5.Create())
            {
                var hashBytes = md5.ComputeHash(data);
                
                var sb = new StringBuilder();
                for (var i = 0; i < hashBytes.Length; i++)
                    sb.Append(hashBytes[i].ToString("x2"));

                return sb.ToString();
            }
        }

        public static string Sha1(this byte[] data)
        {
            using (var sha1 = SHA1.Create())
            {
                var hashBytes = sha1.ComputeHash(data);
                
                var sb = new StringBuilder();
                for (var i = 0; i < hashBytes.Length; i++)
                    sb.Append(hashBytes[i].ToString("x2"));

                return sb.ToString();
            }
        }
    }
}