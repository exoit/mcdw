using System;
using System.Collections.Generic;
using System.IO;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using MCDw.Models.Manifest;
using MCDw.Models.ClientRelease;
using MCDw.Services;
using Newtonsoft.Json;

namespace MCDw
{
    public class Startup : IStartup
    {
        private readonly IFileManager _fileManager;

        private readonly IContentHandler _contentHandler;

        private const int LastReleases = 20; 

        public Startup(IFileManager fileManager, IContentHandler contentHandler)
        {
            _fileManager = fileManager;
            _contentHandler = contentHandler;
        }
        
        public async Task Run()
        {
            var name = GetType().Assembly.GetName().Name;
            var version = GetType().Assembly.GetName().Version;
            
            Console.WriteLine($"Welcome to {name} 'v{version.Major}.{version.Minor}.{version.Build}'");
            
            var manifest = await _contentHandler.GetManifest();

            var latestRelease = manifest.Latest.Release;
            Console.WriteLine($"Latest release '{latestRelease}'");
            Console.WriteLine("Select install version (last 20 releases)");
            
            var releases = manifest.Versions
                    .Where(x => x.Type == "release")
                    .OrderByDescending(x => x.ReleaseTime)
                    .Take(LastReleases)
                    .ToArray();

            var columns = 5;
            var idx = 1;
            foreach (var release in releases)
            {
                var releaseNumber = idx.ToString().PadLeft(2);
                var releaseVersion = release.Id.PadRight(8);
                Console.Write($"{releaseNumber}) {releaseVersion}\t");
                
                if (idx % columns == 0)
                    Console.WriteLine();
                
                idx++;
            }
            Console.WriteLine();
            Console.Write("Select version: ");
            var selectedVersion = Console.ReadLine();

            if (int.TryParse(selectedVersion, out var selectedNumber))
            {
                await InstallSelectedVersion(releases[selectedNumber-1]);
            }
            else
            {
                Console.WriteLine("Error select number");
            }
            
            Console.Write("Complete, press any key for exit...");
            Console.ReadKey();
        }

        private async Task InstallSelectedVersion(ManifestVersion manifestVersion)
        {
            Console.WriteLine($"Process version: '{manifestVersion.Id}'");
            
            var fileName = Path.GetFileNameWithoutExtension(manifestVersion.Url);
            var fileNameWithExtension = Path.GetFileName(manifestVersion.Url);

            var (clientReleaseData, clientRelease) = await _contentHandler.GetClientRelease(manifestVersion);

            var folder = Path.Combine(".", "mc", "versions", fileName);
            await _fileManager.SaveFile(folder, fileNameWithExtension, clientReleaseData);

            var libraries = clientRelease.Libraries
                    .Where(x => x.Natives == null)
                    .Where(x => x.Rules == null || x.Rules.Any(y => y.Os?.Name == null));

            var natives = clientRelease.Libraries
                    .Where(x => x.Natives?.Windows != null);

            await _contentHandler.ProcessClient(clientRelease.Id, clientRelease.Downloads.Client);
            var libraryPaths = await _contentHandler.ProcessLibrary(libraries);
            var nativePaths = await _contentHandler.ProcessNative(clientRelease.Id, natives);

            var paths = new List<string>();
            paths.AddRange(libraryPaths);
            paths.AddRange(nativePaths);
            
            await _contentHandler.ProcessAsset(clientRelease.AssetIndex);
            await _contentHandler.ProcessLaunchScript(clientRelease.Id, clientRelease.AssetIndex.Id, paths);
        }
    }
}