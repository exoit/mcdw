﻿using System;
using System.Reflection;
using MCDw.Services;
using Ninject;

namespace MCDw
{
    class Program
    {
        static void Main(string[] args)
        {
            var kernel = new StandardKernel();
            kernel.Load(Assembly.GetExecutingAssembly());

            kernel.Bind<IStartup>().To<Startup>();
            kernel.Bind<ICommonHttpClient>().To<CommonHttpClient>();
            kernel.Bind<IFileManager>().To<FileManager>();
            kernel.Bind<IContentHandler>().To<ContentHandler>();

            kernel.Get<IStartup>().Run().Wait();
            Console.WriteLine("End");
        }
    }
}