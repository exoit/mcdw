using System.Threading.Tasks;

namespace MCDw
{
    public interface IStartup
    {
        public Task Run();
    }
}