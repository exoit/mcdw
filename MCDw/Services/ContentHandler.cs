using System;
using System.Collections.Generic;
using System.IO;
using System.IO.Compression;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using MCDw.Models.Asset;
using MCDw.Models.ClientRelease;
using MCDw.Models.Manifest;
using MCDw.Utilities;
using Newtonsoft.Json;

namespace MCDw.Services
{
    public class ContentHandler : IContentHandler
    {
        private readonly IFileManager _fileManager;

        private readonly ICommonHttpClient _commonHttpClient;
        
        private static readonly Uri BaseManifestUri = new Uri("https://launchermeta.mojang.com/mc/game/version_manifest.json");

        private static readonly string LibraryUrl = "https://libraries.minecraft.net";

        private static readonly string ResourceUrl = "https://resources.download.minecraft.net";

        public async Task<Manifest> GetManifest()
        {
            var manifest = await _commonHttpClient.GetData(BaseManifestUri);
            var manifestString = Encoding.UTF8.GetString(manifest);
            var manifestObject = JsonConvert.DeserializeObject<Manifest>(manifestString);

            return manifestObject;
        }

        public async Task<(byte[], ClientRelease)> GetClientRelease(ManifestVersion manifestVersion)
        {
            var manifestData = await _commonHttpClient.GetData(manifestVersion.GetUriByUrl);
            var manifestJson = Encoding.UTF8.GetString(manifestData);
            
            var clientRelease = JsonConvert.DeserializeObject<ClientRelease>(manifestJson);

            return (manifestData, clientRelease);
        }
        
        public ContentHandler(IFileManager fileManager, ICommonHttpClient commonHttpClient)
        {
            _fileManager = fileManager;
            _commonHttpClient = commonHttpClient;
        }
        
        public async Task ProcessClient(string version, ClientReleaseArtifact artifact)
        {
            Console.Write($"Client '{version}' ");
            
            var fileName = "client.jar";
            var localPath = Path.Combine(".", "mc", "versions", version);
            var localFilePath = Path.Combine(localPath, fileName);

            if (!File.Exists(localFilePath))
            {
                Console.Write("Download...");
                await DownloadAndSaveFile(artifact.GetUriByUrl, localPath, fileName);
                
                Console.WriteLine("Ok");
            }
            else
            {
                var fileData = await File.ReadAllBytesAsync(localFilePath);
                var fileSha1 = fileData.Sha1();

                if (artifact.Sha1 != fileSha1)
                {
                    Console.Write("Download again...");
                    File.Delete(localFilePath);
                    await DownloadAndSaveFile(artifact.GetUriByUrl, localPath, fileName);
                    
                    Console.WriteLine("Ok");
                }
                else
                {
                    Console.WriteLine("Ok");
                }
            }
        }

        public async Task<IList<string>> ProcessLibrary(IEnumerable<ClientReleaseLibrary> libraries)
        {
            var libraryPaths = new List<string>();
            
            foreach (var library in libraries)
            {
                Console.Write($"Library '{library.Name}' ");
                
                var (path, name, version) = GetInformation(library.Name);
                var uri = new Uri($"{LibraryUrl}/{string.Join("/", path)}/{name}/{version}/{name}-{version}.jar");

                var fileName = $"{name}-{version}.jar";
                var localPath = Path.Combine(".", "mc", "libraries", string.Join(Path.DirectorySeparatorChar, path), name, version);
                var localFilePath = Path.Combine(localPath, fileName);

                if (!File.Exists(localFilePath))
                {
                    Console.Write("Download...");
                    await DownloadAndSaveFile(uri, localPath, fileName);
                    
                    Console.WriteLine("Ok");
                }
                else
                {
                    var fileData = await File.ReadAllBytesAsync(localFilePath);
                    var fileSha1 = fileData.Sha1();

                    if (library.Downloads.Artifact.Sha1 != fileSha1)
                    {
                        Console.Write("Download again...");
                        File.Delete(localFilePath);
                        await DownloadAndSaveFile(uri, localPath, fileName);
                        
                        Console.WriteLine("Ok");
                    }
                    else
                    {
                        Console.WriteLine("Ok");
                    }
                }
                
                libraryPaths.Add(Path.Combine("libraries", string.Join(Path.DirectorySeparatorChar, path), name, version, fileName));
            }

            return libraryPaths;
        }

        private async Task DownloadAndSaveFile(Uri uri, string localPath, string fileName)
        {
            var fileData = await _commonHttpClient.GetData(uri);
            await _fileManager.SaveFile(localPath, fileName, fileData);
        }

        public async Task<IList<string>> ProcessNative(string version, IEnumerable<ClientReleaseLibrary> natives)
        {
            var libraryPaths = new List<string>();
            
            foreach (var native in natives)
            {
                Console.Write($"Native '{native.Name}'");
                
                var (packagePath, packageName, packageVersion) = GetInformation(native.Name);
                var uri = new Uri($"{LibraryUrl}/{string.Join("/", packagePath)}/{packageName}/{packageVersion}/{packageName}-{packageVersion}.jar");
                var localPath = Path.Combine(".", "mc", "libraries", string.Join(Path.DirectorySeparatorChar, packagePath), packageName, packageVersion);
                var libraryData = await _commonHttpClient.GetData(uri);

                await _fileManager.SaveFile(Path.Combine(localPath), $"{packageName}-{packageVersion}.jar", libraryData);

                var nativeArchiveUri = native.Downloads.Classifiers.NativesWindows.GetUriByUrl;
                var nativeArchiveData = await _commonHttpClient.GetData(nativeArchiveUri);
                
                using (var stream = new MemoryStream(nativeArchiveData))
                using (var archive = new ZipArchive(stream))
                {
                    foreach (var entry in archive.Entries)
                    {
                        if (entry.FullName.EndsWith(".dll", StringComparison.OrdinalIgnoreCase))
                        {
                            var destinationPath = Path.Combine(".", "mc", "versions", version, "natives");

                            if (!Directory.Exists(destinationPath))
                                Directory.CreateDirectory(destinationPath);

                            var localFilePath = Path.Combine(destinationPath, entry.FullName);
                            
                            if (!File.Exists(localFilePath))
                                entry.ExtractToFile(localFilePath);
                        }
                    }
                }
                
                libraryPaths.Add(Path.Combine("libraries", string.Join(Path.DirectorySeparatorChar, packagePath), packageName, packageVersion, $"{packageName}-{packageVersion}.jar"));
                Console.WriteLine("...Ok");
            }

            return libraryPaths;
        }

        public async Task ProcessAsset(ClientReleaseAssetIndex assetIndex)
        {
            Console.Write("Assets index ");

            var uri = assetIndex.GetUriByUrl;
            var fileName = assetIndex.Id + ".json";
            var localPath = Path.Combine(".", "mc", "assets", "indexes");
            var localFilePath = Path.Combine(localPath, fileName);

            if (!File.Exists(localFilePath))
            {
                Console.Write("Download...");
                await DownloadAndSaveFile(uri, localPath, fileName);
                    
                Console.WriteLine("Ok");
            }
            else
            {
                var fileData = await File.ReadAllBytesAsync(localFilePath);
                var fileSha1 = fileData.Sha1();

                if (assetIndex.Sha1 != fileSha1)
                {
                    Console.Write("Download again...");
                    File.Delete(localFilePath);
                    await DownloadAndSaveFile(uri, localPath, fileName);
                        
                    Console.WriteLine("Ok");
                }
                else
                {
                    Console.WriteLine("Ok");
                }
            }
            
            var assetsData = await File.ReadAllBytesAsync(localFilePath);
            var assetsString = Encoding.UTF8.GetString(assetsData);
            var assetsObject = JsonConvert.DeserializeObject<Asset>(assetsString);
            var totalAssets = assetsObject.Objects.Count;
            
            Console.Write($"Assets '{totalAssets}'");

            var idx = 1;
            foreach (var asset in assetsObject.Objects)
            {
                Console.Write($"\rAssets '{totalAssets}/{idx}'");
                var assetHash = asset.Value.Hash;
                
                var assetDirectory = $"{assetHash[0]}{assetHash[1]}";
                var assetUri = new Uri($"{ResourceUrl}/{assetDirectory}/{asset.Value.Hash}");

                var assetLocalPath = Path.Combine(".", "mc", "assets", "objects", assetDirectory);
                var assetLocalFilePath = Path.Combine(assetLocalPath, assetHash);

                if (!File.Exists(assetLocalFilePath))
                {
                    await DownloadAndSaveFile(assetUri, assetLocalPath, assetHash);
                }
                else
                {
                    var fileData = await File.ReadAllBytesAsync(assetLocalFilePath);
                    var fileSha1 = fileData.Sha1();
                    
                    if (assetHash != fileSha1)
                    {
                        File.Delete(localFilePath);
                        await DownloadAndSaveFile(uri, localPath, fileName);
                    }
                }
                
                idx++;
            }
            
            Console.WriteLine("...Ok");
        }

        public async Task ProcessLaunchScript(string version, string assetsIndex, IEnumerable<string> libraries)
        {
            var selectedLibraries = new List<string>();
            selectedLibraries.AddRange(libraries);
            selectedLibraries.Add(Path.Combine("versions", version, "client.jar"));
            
            var librariesString = string.Join(";^\n", selectedLibraries);
            var guid = Guid.NewGuid();
            
            var launchParams = new List<string>
            {
                    "java -Xincgc",
                    "-Xmx1024M",
                    "-Xms1024M",
                    "-XX:+AggressiveOpts",
                    "-Dfml.ignoreInvalidMinecraftCertificates=true",
                    "-Dfml.ignorePatchDiscrepancies=true",
                    $"-Djava.library.path={Path.Combine("versions", version, "natives")}",
                    $"-cp {librariesString}",
                    "net.minecraft.client.main.Main",
                    "--username Sample",
                    $"--version {version}",
                    $"--uuid {guid}",
                    $"--accessToken {guid}",
                    "--assetsDir assets",
                    $"--assetIndex {assetsIndex}",
                    "--gameDir ."
            };

            var launchScriptFilePath = Path.Combine(".", "mc", $"Start_{version}.bat");

            if (!File.Exists(launchScriptFilePath))
            {
                var stringLaunchParams = string.Join("^\n ", launchParams);
                await File.WriteAllBytesAsync(launchScriptFilePath, Encoding.UTF8.GetBytes(stringLaunchParams));
            }
        }

        private (IEnumerable<string>, string, string) GetInformation(string name)
        {
            var separatedPackageName = name.Split(":");
            var path = separatedPackageName[0].Split(".");
            var packageName = separatedPackageName[^2];
            var packageVersion = separatedPackageName[^1];

            return (path, packageName, packageVersion);
        }
    }
}