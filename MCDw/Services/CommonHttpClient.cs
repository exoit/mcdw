using System;
using System.Net.Http;
using System.Threading.Tasks;

namespace MCDw.Services
{
    public class CommonHttpClient : ICommonHttpClient
    {
        private readonly HttpClient _httpClient;
        
        public CommonHttpClient(HttpClient httpClient)
        {
            _httpClient = httpClient;
        }

        public async Task<byte[]> GetData(Uri uri)
        {
            var result = await _httpClient.GetAsync(uri);
            return await result.Content.ReadAsByteArrayAsync();
        }
    }
}