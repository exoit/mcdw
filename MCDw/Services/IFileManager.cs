using System.IO;
using System.Threading.Tasks;

namespace MCDw.Services
{
    public interface IFileManager
    {
        public Task SaveFile(string path, string fileName, byte[] fileData);
    }
}