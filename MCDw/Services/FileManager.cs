using System.IO;
using System.Threading.Tasks;

namespace MCDw.Services
{
    public class FileManager : IFileManager
    {
        public async Task SaveFile(string path, string fileName, byte[] fileData)
        {
            if (!Directory.Exists(path))
                Directory.CreateDirectory(path);

            var file = Path.Combine(path, fileName);
            
            if (!File.Exists(file))
                await File.WriteAllBytesAsync(file, fileData);
        }
    }
}