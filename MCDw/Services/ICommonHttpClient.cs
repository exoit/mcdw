using System;
using System.Threading.Tasks;

namespace MCDw.Services
{
    public interface ICommonHttpClient
    {
        public Task<byte[]> GetData(Uri uri);
    }
}