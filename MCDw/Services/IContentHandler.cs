using System.Collections.Generic;
using System.Threading.Tasks;
using MCDw.Models.ClientRelease;
using MCDw.Models.Manifest;

namespace MCDw.Services
{
    public interface IContentHandler
    {
        public Task<Manifest> GetManifest();

        public Task<(byte[], ClientRelease)> GetClientRelease(ManifestVersion manifestVersion);
    
        public Task ProcessClient(string version, ClientReleaseArtifact artifact);

        public Task<IList<string>> ProcessLibrary(IEnumerable<ClientReleaseLibrary> libraries);

        public Task<IList<string>> ProcessNative(string version, IEnumerable<ClientReleaseLibrary> natives);

        public Task ProcessAsset(ClientReleaseAssetIndex assetIndex);

        public Task ProcessLaunchScript(string version, string assetsIndex, IEnumerable<string> libraries);
    }
}