using Newtonsoft.Json;

namespace MCDw.Models.ClientRelease
{
    [JsonObject]
    public class ClientReleaseLibraryRuleOs
    {
        [JsonProperty("name")]
        public string Name { get; set; }
    }
}