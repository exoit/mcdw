using Newtonsoft.Json;

namespace MCDw.Models.ClientRelease
{
    [JsonObject]
    public class ClientReleaseLibraryRule
    {
        [JsonProperty("action")]
        public string Action { get; set; }
        
        [JsonProperty("os")]
        public ClientReleaseLibraryRuleOs Os { get; set; }
    }
}