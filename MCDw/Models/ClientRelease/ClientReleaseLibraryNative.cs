using Newtonsoft.Json;

namespace MCDw.Models.ClientRelease
{
    [JsonObject]
    public class ClientReleaseLibraryNative
    {
        [JsonProperty("linux")]
        public string Linux { get; set; }
        
        [JsonProperty("windows")]
        public string Windows { get; set; }
        
        [JsonProperty("osx")]
        public string Osx { get; set; }
    }
}