using System.Collections.Generic;
using Newtonsoft.Json;

namespace MCDw.Models.ClientRelease
{
    [JsonObject]
    public class ClientReleaseLibraryExtract
    {
        [JsonProperty("exclude")]
        public IList<string> Exclude { get; set; }
    }
}