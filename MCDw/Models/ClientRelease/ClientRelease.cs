using System;
using System.Collections.Generic;
using Newtonsoft.Json;
using Newtonsoft.Json.Linq;

namespace MCDw.Models.ClientRelease
{
    [JsonObject]
    public class ClientRelease
    {
        [JsonProperty("arguments")]
        [JsonIgnore]
        public JObject Arguments { get; set; }
        
        [JsonProperty("javaVersion")]
        [JsonIgnore]
        public JObject JavaVersion { get; set; }
        
        [JsonProperty("assets")]
        public string Assets { get; set; }
        
        [JsonProperty("assetIndex")]
        public ClientReleaseAssetIndex AssetIndex { get; set; }

        [JsonProperty("complianceLevel")]
        public int? ComplianceLevel { get; set; }
        
        [JsonProperty("id")]
        public string Id { get; set; }
        
        [JsonProperty("downloads")]
        public ClientReleaseDownload Downloads { get; set; }
        
        [JsonProperty("libraries")]
        public IList<ClientReleaseLibrary> Libraries { get; set; }
        
        [JsonProperty("logging")]
        [JsonIgnore]
        public JObject Logging { get; set; }
        
        [JsonProperty("mainClass")]
        public string MainClass { get; set; }
        
        [JsonProperty("minimumLauncherVersion")]
        public int? MinimumLauncherVersion { get; set; }
        
        [JsonProperty("releaseTime")]
        public DateTimeOffset ReleaseTime { get; set; }
        
        [JsonProperty("time")]
        public DateTimeOffset Time { get; set; }
        
        [JsonProperty("type")]
        public string Type { get; set; }
    }
}