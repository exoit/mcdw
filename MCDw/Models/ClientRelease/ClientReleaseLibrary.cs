using System.Collections.Generic;
using Newtonsoft.Json;

namespace MCDw.Models.ClientRelease
{
    [JsonObject]
    public class ClientReleaseLibrary
    {
        [JsonProperty("downloads")]
        public ClientReleaseLibraryDownload Downloads { get; set; }
        
        [JsonProperty("name")]
        public string Name { get; set; }
        
        [JsonProperty("rules")]
        public IList<ClientReleaseLibraryRule> Rules { get; set; }
        
        [JsonProperty("natives")]
        public ClientReleaseLibraryNative Natives { get; set; }
        
        [JsonProperty("extract")]
        public ClientReleaseLibraryExtract Extract { get; set; }
    }
}