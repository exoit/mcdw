using System;
using Newtonsoft.Json;

namespace MCDw.Models.ClientRelease
{
    [JsonObject]
    public class ClientReleaseAssetIndex
    {
        [JsonProperty("id")]
        public string Id { get; set; }
        
        [JsonProperty("sha1")]
        public string Sha1 { get; set; }
        
        [JsonProperty("size")]
        public int Size { get; set; }
        
        [JsonProperty("totalSize")]
        public int TotalSize { get; set; }
        
        [JsonProperty("url")]
        public string Url { get; set; }

        public Uri GetUriByUrl => new Uri(Url);
    }
}