using Newtonsoft.Json;

namespace MCDw.Models.ClientRelease
{
    [JsonObject]
    public class ClientReleaseLibraryDownloadClassifiers
    {
        [JsonProperty("javadoc")]
        public ClientReleaseArtifact JavaDoc { get; set; }
        
        [JsonProperty("natives-linux")]
        public ClientReleaseArtifact NativesLinux { get; set; }
        
        [JsonProperty("natives-macos")]
        public ClientReleaseArtifact NativesMacos { get; set; }
        
        [JsonProperty("natives-windows")]
        public ClientReleaseArtifact NativesWindows { get; set; }
        
        [JsonProperty("sources")]
        public ClientReleaseArtifact Sources { get; set; }
        
        [JsonProperty("natives-osx")]
        public ClientReleaseArtifact NativesOsx { get; set; }
    }
}