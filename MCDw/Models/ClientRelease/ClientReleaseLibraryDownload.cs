using Newtonsoft.Json;

namespace MCDw.Models.ClientRelease
{
    [JsonObject]
    public class ClientReleaseLibraryDownload
    {
        [JsonProperty("artifact")]
        public ClientReleaseArtifact Artifact { get; set; }
        
        [JsonProperty("classifiers")]
        public ClientReleaseLibraryDownloadClassifiers Classifiers { get; set; }
    }
}