using Newtonsoft.Json;

namespace MCDw.Models.ClientRelease
{
    [JsonObject]
    public class ClientReleaseDownload
    {
        [JsonProperty("client")]
        public ClientReleaseArtifact Client { get; set; }
        
        [JsonProperty("client_mappings")]
        public ClientReleaseArtifact ClientMappings { get; set; }
        
        [JsonProperty("server")]
        public ClientReleaseArtifact Server { get; set; }
        
        [JsonProperty("server_mappings")]
        public ClientReleaseArtifact ServerMappings { get; set; }
    }
}