using System;
using Newtonsoft.Json;

namespace MCDw.Models.ClientRelease
{
    [JsonObject]
    public class ClientReleaseArtifact
    {
        [JsonProperty("sha1")]
        public string Sha1 { get; set; }
        
        [JsonProperty("size")]
        public int Size { get; set; }
        
        [JsonProperty("url")]
        public string Url { get; set; }
        
        [JsonProperty("path")]
        public string Path { get; set; }
        
        public Uri GetUriByUrl => new Uri(Url);
    }
}