using System.Collections.Generic;
using Newtonsoft.Json;

namespace MCDw.Models.Manifest
{
    [JsonObject]
    public class Manifest
    {
        [JsonProperty("latest")]
        public ManifestLatest Latest { get; set; }
        
        [JsonProperty("versions")]
        public IList<ManifestVersion> Versions { get; set; }
    }
}