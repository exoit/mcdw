using System;
using Newtonsoft.Json;

namespace MCDw.Models.Manifest
{
    [JsonObject]
    public class ManifestVersion
    {
        [JsonProperty("id")]
        public string Id { get; set; }
        
        [JsonProperty("type")]
        public string Type { get; set; }
        
        [JsonProperty("url")]
        public string Url { get; set; }
        
        [JsonProperty("time")]
        public DateTimeOffset Time { get; set; }
        
        [JsonProperty("releaseTime")]
        public DateTimeOffset ReleaseTime { get; set; }
        
        public Uri GetUriByUrl => new Uri(Url);
    }
}