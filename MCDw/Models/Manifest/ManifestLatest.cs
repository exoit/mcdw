using Newtonsoft.Json;

namespace MCDw.Models.Manifest
{
    [JsonObject]
    public class ManifestLatest
    {
        [JsonProperty("release")]
        public string Release { get; set; }
        
        [JsonProperty("snapshot")]
        public string Snapshot { get; set; }
    }
}