using System.Collections.Generic;
using Newtonsoft.Json;

namespace MCDw.Models.Asset
{
    [JsonObject]
    public class Asset
    {
        [JsonProperty("objects")]
        public Dictionary<string, AssetItem> Objects { get; set; }
    }
}