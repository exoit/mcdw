using Newtonsoft.Json;

namespace MCDw.Models.Asset
{
    [JsonObject]
    public class AssetItem
    {
        [JsonProperty("hash")]
        public string Hash { get; set; }
        
        [JsonProperty("size")]
        public int Size { get; set; }
    }
}