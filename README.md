## What it's is
This is a simple minecraft launcher (console installer) for Windows x32/x64

## Requirements
- Windows 10 x32/x64
- NET Core Runtime >= v5.0 [download here](https://dotnet.microsoft.com/download/dotnet/5.0/runtime)
- Java >= 1.8 for Minecraft <= v1.16 [download here](https://java.com/en/download/manual.jsp)

## Features
- Download Minecraft Client (libraries and another resources)
- Make launch script (.bat file) for start game

## Our plans
- (In future) fully support Minecraft >= v1.17
- (In future) optionally download OpenJDK/GraalVM
- (In future) optimized GUI
- (In future) support other OS

## Community
We are always open to your suggestions